//Логин для входа в веб-интерфейс роутера
const Login='admin';
//Пароль
const Password='1234';
//Локальный IP роутера
const RouterIP='192.168.1.1';
//Интервал смены IP в часах
const Interval=3;

var i:=0;
//Бесконечный цикл
while i=0 do
  begin
    //Ожидание Interval часов перед сменой IP
    Wait(Interval*3600000);
    //Заходим в веб интерфейс
    LoadURI('http://'+Login+':'+Password+'@'+RouterIP);
    //Заходим во вкладку "Интернет"
    ClickElement(GetElement('a',0,['href'],['clickOnFolder(1)']));
    Wait(1000);
    //Заходим во вкладку "Авторизация"
    ClickElement(GetElement('a',0,['href'],['/internet/ppp.asp']));
    Wait(1000);
    //Нажимаем кнопку "Отключить"
    ClickElement(GetElement('input',0,['id'],['disconnect']));
    Wait(5000);
    //Нажимаем кнопку "Подключить"
    ClickElement(GetElement('input',0,['id'],['save_connect']));
  end;