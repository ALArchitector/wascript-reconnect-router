//Логин для входа в веб-интерфейс роутера
const Login='admin';
//Пароль
const Password='1234';
//Локальный IP роутера
const RouterIP='192.168.1.1';
//Интервал смены IP в часах
const Interval=3;

LoadURI(RouterIP);
var LoginField:=GetElement('input',0,['name'],['login_n'],true);
ClickElement(LoginField);
TypeIn(Login);
var PasswordField:=GetElement('input',0,['name'],['login_pass'],true);
ClickElement(PasswordField);
TypeIn(Password);
var Submit:=GetElement('input',0,['name'],['login'],true);
ClickElement(Submit);
Wait(5000);
var Status:=GetElement('a',0,['href'],['status.htm'],true);
ClickElement(Status);
Wait(5000);
var Disconnect:=GetElement('input',0,['type','name'],['button','disconnect'],true);
ClickElement(Disconnect);
Wait(5000);
var Connect:=GetElement('input',0,['type','name'],['button','connect'],true);
ClickElement(Connect);
Wait(5000);
var i:=0;
while i=0 do
begin
  Wait(Interval*3600000);
  Disconnect:=GetElement('input',0,['type','name'],['button','disconnect'],true);
  ClickElement(Disconnect);
  Wait(5000);
  Connect:=GetElement('input',0,['type','name'],['button','connect'],true);
  ClickElement(Connect);
end;
